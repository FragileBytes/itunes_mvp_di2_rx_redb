// Generated code from Butter Knife. Do not modify!
package com.fragilebytes.itunes_mvp_di2_rx_redb.view.activity;

import android.content.res.Resources;
import butterknife.ButterKnife.Finder;

public class MainActivity$$ViewBinder<T extends com.fragilebytes.itunes_mvp_di2_rx_redb.view.activity.MainActivity> extends com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BaseActivity$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    Resources res = finder.getContext(source).getResources();
    target.tag = res.getString(2131099672);
  }

  @Override public void unbind(T target) {
    super.unbind(target);

  }
}
