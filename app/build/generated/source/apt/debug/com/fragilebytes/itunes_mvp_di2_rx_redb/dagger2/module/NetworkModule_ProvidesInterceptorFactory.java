package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvidesInterceptorFactory implements Factory<HttpLoggingInterceptor> {
  private final NetworkModule module;

  public NetworkModule_ProvidesInterceptorFactory(NetworkModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public HttpLoggingInterceptor get() {  
    HttpLoggingInterceptor provided = module.providesInterceptor();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<HttpLoggingInterceptor> create(NetworkModule module) {  
    return new NetworkModule_ProvidesInterceptorFactory(module);
  }
}

