package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.retrofit.ItunesApiService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class InteractorModule_ProvideInteractorFactory implements Factory<ArtistInteractor> {
  private final InteractorModule module;
  private final Provider<ItunesApiService> apiServiceProvider;

  public InteractorModule_ProvideInteractorFactory(InteractorModule module, Provider<ItunesApiService> apiServiceProvider) {  
    assert module != null;
    this.module = module;
    assert apiServiceProvider != null;
    this.apiServiceProvider = apiServiceProvider;
  }

  @Override
  public ArtistInteractor get() {  
    ArtistInteractor provided = module.provideInteractor(apiServiceProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ArtistInteractor> create(InteractorModule module, Provider<ItunesApiService> apiServiceProvider) {  
    return new InteractorModule_ProvideInteractorFactory(module, apiServiceProvider);
  }
}

