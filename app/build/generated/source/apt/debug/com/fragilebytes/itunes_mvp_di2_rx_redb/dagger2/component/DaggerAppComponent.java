package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerAppComponent implements AppComponent {
  private DaggerAppComponent(Builder builder) {  
    assert builder != null;
  }

  public static Builder builder() {  
    return new Builder();
  }

  public static AppComponent create() {  
    return builder().build();
  }

  public static final class Builder {
    private Builder() {  
    }
  
    public AppComponent build() {  
      return new DaggerAppComponent(this);
    }
  }
}

