package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.RealmPresenter;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class RealmPresenterModule_ProvidePresenterFactory implements Factory<RealmPresenter> {
  private final RealmPresenterModule module;
  private final Provider<RealmInteractor> interactorProvider;

  public RealmPresenterModule_ProvidePresenterFactory(RealmPresenterModule module, Provider<RealmInteractor> interactorProvider) {  
    assert module != null;
    this.module = module;
    assert interactorProvider != null;
    this.interactorProvider = interactorProvider;
  }

  @Override
  public RealmPresenter get() {  
    RealmPresenter provided = module.providePresenter(interactorProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<RealmPresenter> create(RealmPresenterModule module, Provider<RealmInteractor> interactorProvider) {  
    return new RealmPresenterModule_ProvidePresenterFactory(module, interactorProvider);
  }
}

