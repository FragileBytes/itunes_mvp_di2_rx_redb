package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.google.gson.Gson;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvidesGsonFactory implements Factory<Gson> {
  private final NetworkModule module;

  public NetworkModule_ProvidesGsonFactory(NetworkModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public Gson get() {  
    Gson provided = module.providesGson();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<Gson> create(NetworkModule module) {  
    return new NetworkModule_ProvidesGsonFactory(module);
  }
}

