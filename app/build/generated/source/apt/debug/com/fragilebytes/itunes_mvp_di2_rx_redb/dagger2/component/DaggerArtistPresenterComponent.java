package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import android.content.Context;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule_ProvideContextFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ArtistPresenterModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ArtistPresenterModule_ProvideAdapterFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ArtistPresenterModule_ProvidePresenterFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ArtistPresenterModule_ProvideViewModelFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmPresenterModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmPresenterModule_ProvidePresenterFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.RealmPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.activity.MainActivity;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter.ArtistAdapter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment.ArtistListFragment;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment.ArtistListFragment_MembersInjector;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.viewmodel.ArtistViewModel;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerArtistPresenterComponent implements ArtistPresenterComponent {
  private Provider<ArtistInteractor> getArtistInteractorProvider;
  private Provider<RealmInteractor> getRealmInteractorProvider;
  private Provider<ArtistViewModel> provideViewModelProvider;
  private Provider<ArtistPresenter> providePresenterProvider;
  private Provider<Context> provideContextProvider;
  private Provider<ArtistAdapter> provideAdapterProvider;
  private Provider<RealmPresenter> providePresenterProvider1;
  private MembersInjector<ArtistListFragment> artistListFragmentMembersInjector;

  private DaggerArtistPresenterComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  private void initialize(final Builder builder) {  
    this.getArtistInteractorProvider = new Factory<ArtistInteractor>() {
      @Override public ArtistInteractor get() {
        ArtistInteractor provided = builder.networkComponent.getArtistInteractor();
        if (provided == null) {
          throw new NullPointerException("Cannot return null from a non-@Nullable component method");
        }
        return provided;
      }
    };
    this.getRealmInteractorProvider = new Factory<RealmInteractor>() {
      @Override public RealmInteractor get() {
        RealmInteractor provided = builder.realmComponent.getRealmInteractor();
        if (provided == null) {
          throw new NullPointerException("Cannot return null from a non-@Nullable component method");
        }
        return provided;
      }
    };
    this.provideViewModelProvider = ArtistPresenterModule_ProvideViewModelFactory.create(builder.artistPresenterModule);
    this.providePresenterProvider = ArtistPresenterModule_ProvidePresenterFactory.create(builder.artistPresenterModule, getArtistInteractorProvider, provideViewModelProvider);
    this.provideContextProvider = ApplicationModule_ProvideContextFactory.create(builder.applicationModule);
    this.provideAdapterProvider = ArtistPresenterModule_ProvideAdapterFactory.create(builder.artistPresenterModule, provideContextProvider);
    this.providePresenterProvider1 = RealmPresenterModule_ProvidePresenterFactory.create(builder.realmPresenterModule, getRealmInteractorProvider);
    this.artistListFragmentMembersInjector = ArtistListFragment_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), providePresenterProvider, provideAdapterProvider, providePresenterProvider1);
  }

  @Override
  public ArtistInteractor getArtistInteractor() {  
    return getArtistInteractorProvider.get();
  }

  @Override
  public RealmInteractor getRealmInteractor() {  
    return getRealmInteractorProvider.get();
  }

  @Override
  public void inject(MainActivity activity) {  
    MembersInjectors.noOp().injectMembers(activity);
  }

  @Override
  public void inject(ArtistListFragment fragment) {  
    artistListFragmentMembersInjector.injectMembers(fragment);
  }

  @Override
  public ArtistPresenter getPresenter() {  
    return providePresenterProvider.get();
  }

  @Override
  public ArtistAdapter getAdapter() {  
    return provideAdapterProvider.get();
  }

  @Override
  public RealmPresenter getRealmPresenter() {  
    return providePresenterProvider1.get();
  }

  public static final class Builder {
    private ArtistPresenterModule artistPresenterModule;
    private ApplicationModule applicationModule;
    private RealmPresenterModule realmPresenterModule;
    private NetworkComponent networkComponent;
    private RealmComponent realmComponent;
  
    private Builder() {  
    }
  
    public ArtistPresenterComponent build() {  
      if (artistPresenterModule == null) {
        throw new IllegalStateException("artistPresenterModule must be set");
      }
      if (applicationModule == null) {
        throw new IllegalStateException("applicationModule must be set");
      }
      if (realmPresenterModule == null) {
        this.realmPresenterModule = new RealmPresenterModule();
      }
      if (networkComponent == null) {
        throw new IllegalStateException("networkComponent must be set");
      }
      if (realmComponent == null) {
        throw new IllegalStateException("realmComponent must be set");
      }
      return new DaggerArtistPresenterComponent(this);
    }
  
    public Builder artistPresenterModule(ArtistPresenterModule artistPresenterModule) {  
      if (artistPresenterModule == null) {
        throw new NullPointerException("artistPresenterModule");
      }
      this.artistPresenterModule = artistPresenterModule;
      return this;
    }
  
    public Builder applicationModule(ApplicationModule applicationModule) {  
      if (applicationModule == null) {
        throw new NullPointerException("applicationModule");
      }
      this.applicationModule = applicationModule;
      return this;
    }
  
    public Builder realmPresenterModule(RealmPresenterModule realmPresenterModule) {  
      if (realmPresenterModule == null) {
        throw new NullPointerException("realmPresenterModule");
      }
      this.realmPresenterModule = realmPresenterModule;
      return this;
    }
  
    public Builder networkComponent(NetworkComponent networkComponent) {  
      if (networkComponent == null) {
        throw new NullPointerException("networkComponent");
      }
      this.networkComponent = networkComponent;
      return this;
    }
  
    public Builder realmComponent(RealmComponent realmComponent) {  
      if (realmComponent == null) {
        throw new NullPointerException("realmComponent");
      }
      this.realmComponent = realmComponent;
      return this;
    }
  }
}

