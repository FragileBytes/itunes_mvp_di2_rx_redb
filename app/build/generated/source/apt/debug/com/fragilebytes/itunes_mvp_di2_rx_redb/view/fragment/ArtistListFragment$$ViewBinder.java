// Generated code from Butter Knife. Do not modify!
package com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ArtistListFragment$$ViewBinder<T extends com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment.ArtistListFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492984, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131492984, "field 'recyclerView'");
    view = finder.findRequiredView(source, 2131492983, "field 'editText'");
    target.editText = finder.castView(view, 2131492983, "field 'editText'");
  }

  @Override public void unbind(T target) {
    target.recyclerView = null;
    target.editText = null;
  }
}
