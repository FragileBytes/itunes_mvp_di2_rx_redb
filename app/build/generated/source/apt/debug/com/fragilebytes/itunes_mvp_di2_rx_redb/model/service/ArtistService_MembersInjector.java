package com.fragilebytes.itunes_mvp_di2_rx_redb.model.service;

import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BaseService;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ArtistService_MembersInjector implements MembersInjector<ArtistService> {
  private final MembersInjector<BaseService> supertypeInjector;
  private final Provider<ArtistPresenter> presenterProvider;

  public ArtistService_MembersInjector(MembersInjector<BaseService> supertypeInjector, Provider<ArtistPresenter> presenterProvider) {  
    assert supertypeInjector != null;
    this.supertypeInjector = supertypeInjector;
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  @Override
  public void injectMembers(ArtistService instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    supertypeInjector.injectMembers(instance);
    instance.presenter = presenterProvider.get();
  }

  public static MembersInjector<ArtistService> create(MembersInjector<BaseService> supertypeInjector, Provider<ArtistPresenter> presenterProvider) {  
      return new ArtistService_MembersInjector(supertypeInjector, presenterProvider);
  }
}

