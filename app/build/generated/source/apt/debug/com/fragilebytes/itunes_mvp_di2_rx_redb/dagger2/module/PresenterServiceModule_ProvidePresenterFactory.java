package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class PresenterServiceModule_ProvidePresenterFactory implements Factory<ArtistPresenter> {
  private final PresenterServiceModule module;
  private final Provider<ArtistInteractor> interactorProvider;

  public PresenterServiceModule_ProvidePresenterFactory(PresenterServiceModule module, Provider<ArtistInteractor> interactorProvider) {  
    assert module != null;
    this.module = module;
    assert interactorProvider != null;
    this.interactorProvider = interactorProvider;
  }

  @Override
  public ArtistPresenter get() {  
    ArtistPresenter provided = module.providePresenter(interactorProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ArtistPresenter> create(PresenterServiceModule module, Provider<ArtistInteractor> interactorProvider) {  
    return new PresenterServiceModule_ProvidePresenterFactory(module, interactorProvider);
  }
}

