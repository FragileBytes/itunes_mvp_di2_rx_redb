package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvidesOkHttpClientFactory implements Factory<OkHttpClient> {
  private final NetworkModule module;
  private final Provider<HttpLoggingInterceptor> interceptorProvider;

  public NetworkModule_ProvidesOkHttpClientFactory(NetworkModule module, Provider<HttpLoggingInterceptor> interceptorProvider) {  
    assert module != null;
    this.module = module;
    assert interceptorProvider != null;
    this.interceptorProvider = interceptorProvider;
  }

  @Override
  public OkHttpClient get() {  
    OkHttpClient provided = module.providesOkHttpClient(interceptorProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<OkHttpClient> create(NetworkModule module, Provider<HttpLoggingInterceptor> interceptorProvider) {  
    return new NetworkModule_ProvidesOkHttpClientFactory(module, interceptorProvider);
  }
}

