package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealmDAO;
import dagger.internal.Factory;
import io.realm.Realm;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class RealmModule_ProvideArtistRealmDAOFactory implements Factory<ArtistRealmDAO> {
  private final RealmModule module;
  private final Provider<Realm> realmProvider;

  public RealmModule_ProvideArtistRealmDAOFactory(RealmModule module, Provider<Realm> realmProvider) {  
    assert module != null;
    this.module = module;
    assert realmProvider != null;
    this.realmProvider = realmProvider;
  }

  @Override
  public ArtistRealmDAO get() {  
    ArtistRealmDAO provided = module.provideArtistRealmDAO(realmProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ArtistRealmDAO> create(RealmModule module, Provider<Realm> realmProvider) {  
    return new RealmModule_ProvideArtistRealmDAOFactory(module, realmProvider);
  }
}

