// Generated code from Butter Knife. Do not modify!
package com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ArtistAdapter$ViewHolder$$ViewBinder<T extends com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter.ArtistAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492978, "field 'imageView'");
    target.imageView = finder.castView(view, 2131492978, "field 'imageView'");
    target.textViews = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131492979, "field 'textViews'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131492980, "field 'textViews'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131492981, "field 'textViews'")
    );
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.textViews = null;
  }
}
