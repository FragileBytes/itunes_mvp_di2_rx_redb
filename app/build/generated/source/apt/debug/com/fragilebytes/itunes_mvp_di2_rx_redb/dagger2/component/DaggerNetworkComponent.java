package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.InteractorModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.InteractorModule_ProvideInteractorFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule_ProvidesGsonFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule_ProvidesInterceptorFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule_ProvidesItunesApiServiceFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule_ProvidesOkHttpClientFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule_ProvidesRetrofitFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.retrofit.ItunesApiService;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit.Retrofit;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerNetworkComponent implements NetworkComponent {
  private Provider<HttpLoggingInterceptor> providesInterceptorProvider;
  private Provider<OkHttpClient> providesOkHttpClientProvider;
  private Provider<Gson> providesGsonProvider;
  private Provider<Retrofit> providesRetrofitProvider;
  private Provider<ItunesApiService> providesItunesApiServiceProvider;
  private Provider<ArtistInteractor> provideInteractorProvider;

  private DaggerNetworkComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  public static NetworkComponent create() {  
    return builder().build();
  }

  private void initialize(final Builder builder) {  
    this.providesInterceptorProvider = NetworkModule_ProvidesInterceptorFactory.create(builder.networkModule);
    this.providesOkHttpClientProvider = NetworkModule_ProvidesOkHttpClientFactory.create(builder.networkModule, providesInterceptorProvider);
    this.providesGsonProvider = NetworkModule_ProvidesGsonFactory.create(builder.networkModule);
    this.providesRetrofitProvider = NetworkModule_ProvidesRetrofitFactory.create(builder.networkModule, providesOkHttpClientProvider, providesGsonProvider);
    this.providesItunesApiServiceProvider = NetworkModule_ProvidesItunesApiServiceFactory.create(builder.networkModule, providesRetrofitProvider);
    this.provideInteractorProvider = InteractorModule_ProvideInteractorFactory.create(builder.interactorModule, providesItunesApiServiceProvider);
  }

  @Override
  public ArtistInteractor getArtistInteractor() {  
    return provideInteractorProvider.get();
  }

  public static final class Builder {
    private NetworkModule networkModule;
    private InteractorModule interactorModule;
  
    private Builder() {  
    }
  
    public NetworkComponent build() {  
      if (networkModule == null) {
        this.networkModule = new NetworkModule();
      }
      if (interactorModule == null) {
        this.interactorModule = new InteractorModule();
      }
      return new DaggerNetworkComponent(this);
    }
  
    public Builder networkModule(NetworkModule networkModule) {  
      if (networkModule == null) {
        throw new NullPointerException("networkModule");
      }
      this.networkModule = networkModule;
      return this;
    }
  
    public Builder interactorModule(InteractorModule interactorModule) {  
      if (interactorModule == null) {
        throw new NullPointerException("interactorModule");
      }
      this.interactorModule = interactorModule;
      return this;
    }
  }
}

