package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealmDAO;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class RealmInteractorModule_ProvideRealmInteractorFactory implements Factory<RealmInteractor> {
  private final RealmInteractorModule module;
  private final Provider<ArtistRealmDAO> realmDAOProvider;

  public RealmInteractorModule_ProvideRealmInteractorFactory(RealmInteractorModule module, Provider<ArtistRealmDAO> realmDAOProvider) {  
    assert module != null;
    this.module = module;
    assert realmDAOProvider != null;
    this.realmDAOProvider = realmDAOProvider;
  }

  @Override
  public RealmInteractor get() {  
    RealmInteractor provided = module.provideRealmInteractor(realmDAOProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<RealmInteractor> create(RealmInteractorModule module, Provider<ArtistRealmDAO> realmDAOProvider) {  
    return new RealmInteractorModule_ProvideRealmInteractorFactory(module, realmDAOProvider);
  }
}

