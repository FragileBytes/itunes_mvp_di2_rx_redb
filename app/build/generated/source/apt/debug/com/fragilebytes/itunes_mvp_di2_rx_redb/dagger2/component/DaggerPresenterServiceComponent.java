package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.PresenterServiceModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.PresenterServiceModule_ProvidePresenterFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.service.ArtistService;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.service.ArtistService_MembersInjector;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerPresenterServiceComponent implements PresenterServiceComponent {
  private Provider<ArtistInteractor> getArtistInteractorProvider;
  private Provider<ArtistPresenter> providePresenterProvider;
  private MembersInjector<ArtistService> artistServiceMembersInjector;

  private DaggerPresenterServiceComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  private void initialize(final Builder builder) {  
    this.getArtistInteractorProvider = new Factory<ArtistInteractor>() {
      @Override public ArtistInteractor get() {
        ArtistInteractor provided = builder.networkComponent.getArtistInteractor();
        if (provided == null) {
          throw new NullPointerException("Cannot return null from a non-@Nullable component method");
        }
        return provided;
      }
    };
    this.providePresenterProvider = PresenterServiceModule_ProvidePresenterFactory.create(builder.presenterServiceModule, getArtistInteractorProvider);
    this.artistServiceMembersInjector = ArtistService_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), providePresenterProvider);
  }

  @Override
  public void inject(ArtistService service) {  
    artistServiceMembersInjector.injectMembers(service);
  }

  @Override
  public ArtistPresenter getPresenter() {  
    return providePresenterProvider.get();
  }

  public static final class Builder {
    private PresenterServiceModule presenterServiceModule;
    private NetworkComponent networkComponent;
  
    private Builder() {  
    }
  
    public PresenterServiceComponent build() {  
      if (presenterServiceModule == null) {
        this.presenterServiceModule = new PresenterServiceModule();
      }
      if (networkComponent == null) {
        throw new IllegalStateException("networkComponent must be set");
      }
      return new DaggerPresenterServiceComponent(this);
    }
  
    public Builder presenterServiceModule(PresenterServiceModule presenterServiceModule) {  
      if (presenterServiceModule == null) {
        throw new NullPointerException("presenterServiceModule");
      }
      this.presenterServiceModule = presenterServiceModule;
      return this;
    }
  
    public Builder networkComponent(NetworkComponent networkComponent) {  
      if (networkComponent == null) {
        throw new NullPointerException("networkComponent");
      }
      this.networkComponent = networkComponent;
      return this;
    }
  }
}

