package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import android.content.Context;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule_ProvideContextFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmInteractorModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmInteractorModule_ProvideRealmInteractorFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmModule_ProvideArtistRealmDAOFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmModule_ProvideRealmFactory;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealmDAO;
import io.realm.Realm;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerRealmComponent implements RealmComponent {
  private Provider<Context> provideContextProvider;
  private Provider<Realm> provideRealmProvider;
  private Provider<ArtistRealmDAO> provideArtistRealmDAOProvider;
  private Provider<RealmInteractor> provideRealmInteractorProvider;

  private DaggerRealmComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  private void initialize(final Builder builder) {  
    this.provideContextProvider = ApplicationModule_ProvideContextFactory.create(builder.applicationModule);
    this.provideRealmProvider = RealmModule_ProvideRealmFactory.create(builder.realmModule, provideContextProvider);
    this.provideArtistRealmDAOProvider = RealmModule_ProvideArtistRealmDAOFactory.create(builder.realmModule, provideRealmProvider);
    this.provideRealmInteractorProvider = RealmInteractorModule_ProvideRealmInteractorFactory.create(builder.realmInteractorModule, provideArtistRealmDAOProvider);
  }

  @Override
  public RealmInteractor getRealmInteractor() {  
    return provideRealmInteractorProvider.get();
  }

  public static final class Builder {
    private RealmModule realmModule;
    private RealmInteractorModule realmInteractorModule;
    private ApplicationModule applicationModule;
  
    private Builder() {  
    }
  
    public RealmComponent build() {  
      if (realmModule == null) {
        this.realmModule = new RealmModule();
      }
      if (realmInteractorModule == null) {
        this.realmInteractorModule = new RealmInteractorModule();
      }
      if (applicationModule == null) {
        throw new IllegalStateException("applicationModule must be set");
      }
      return new DaggerRealmComponent(this);
    }
  
    public Builder realmModule(RealmModule realmModule) {  
      if (realmModule == null) {
        throw new NullPointerException("realmModule");
      }
      this.realmModule = realmModule;
      return this;
    }
  
    public Builder realmInteractorModule(RealmInteractorModule realmInteractorModule) {  
      if (realmInteractorModule == null) {
        throw new NullPointerException("realmInteractorModule");
      }
      this.realmInteractorModule = realmInteractorModule;
      return this;
    }
  
    public Builder applicationModule(ApplicationModule applicationModule) {  
      if (applicationModule == null) {
        throw new NullPointerException("applicationModule");
      }
      this.applicationModule = applicationModule;
      return this;
    }
  }
}

