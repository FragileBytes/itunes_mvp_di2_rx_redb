package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.view.viewmodel.ArtistViewModel;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ArtistPresenterModule_ProvideViewModelFactory implements Factory<ArtistViewModel> {
  private final ArtistPresenterModule module;

  public ArtistPresenterModule_ProvideViewModelFactory(ArtistPresenterModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public ArtistViewModel get() {  
    ArtistViewModel provided = module.provideViewModel();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ArtistViewModel> create(ArtistPresenterModule module) {  
    return new ArtistPresenterModule_ProvideViewModelFactory(module);
  }
}

