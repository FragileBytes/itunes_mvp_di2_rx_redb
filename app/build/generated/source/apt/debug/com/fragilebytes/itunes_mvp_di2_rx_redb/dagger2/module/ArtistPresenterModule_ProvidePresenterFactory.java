package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.viewmodel.ArtistViewModel;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ArtistPresenterModule_ProvidePresenterFactory implements Factory<ArtistPresenter> {
  private final ArtistPresenterModule module;
  private final Provider<ArtistInteractor> interactorProvider;
  private final Provider<ArtistViewModel> viewModelProvider;

  public ArtistPresenterModule_ProvidePresenterFactory(ArtistPresenterModule module, Provider<ArtistInteractor> interactorProvider, Provider<ArtistViewModel> viewModelProvider) {  
    assert module != null;
    this.module = module;
    assert interactorProvider != null;
    this.interactorProvider = interactorProvider;
    assert viewModelProvider != null;
    this.viewModelProvider = viewModelProvider;
  }

  @Override
  public ArtistPresenter get() {  
    ArtistPresenter provided = module.providePresenter(interactorProvider.get(), viewModelProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ArtistPresenter> create(ArtistPresenterModule module, Provider<ArtistInteractor> interactorProvider, Provider<ArtistViewModel> viewModelProvider) {  
    return new ArtistPresenterModule_ProvidePresenterFactory(module, interactorProvider, viewModelProvider);
  }
}

