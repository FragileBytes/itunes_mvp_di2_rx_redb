package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.content.Context;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter.ArtistAdapter;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ArtistPresenterModule_ProvideAdapterFactory implements Factory<ArtistAdapter> {
  private final ArtistPresenterModule module;
  private final Provider<Context> contextProvider;

  public ArtistPresenterModule_ProvideAdapterFactory(ArtistPresenterModule module, Provider<Context> contextProvider) {  
    assert module != null;
    this.module = module;
    assert contextProvider != null;
    this.contextProvider = contextProvider;
  }

  @Override
  public ArtistAdapter get() {  
    ArtistAdapter provided = module.provideAdapter(contextProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ArtistAdapter> create(ArtistPresenterModule module, Provider<Context> contextProvider) {  
    return new ArtistPresenterModule_ProvideAdapterFactory(module, contextProvider);
  }
}

