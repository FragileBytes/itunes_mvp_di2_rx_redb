package com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment;

import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.RealmPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter.ArtistAdapter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BaseFragment;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ArtistListFragment_MembersInjector implements MembersInjector<ArtistListFragment> {
  private final MembersInjector<BaseFragment> supertypeInjector;
  private final Provider<ArtistPresenter> presenterProvider;
  private final Provider<ArtistAdapter> adapterProvider;
  private final Provider<RealmPresenter> realmPresenterProvider;

  public ArtistListFragment_MembersInjector(MembersInjector<BaseFragment> supertypeInjector, Provider<ArtistPresenter> presenterProvider, Provider<ArtistAdapter> adapterProvider, Provider<RealmPresenter> realmPresenterProvider) {  
    assert supertypeInjector != null;
    this.supertypeInjector = supertypeInjector;
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
    assert adapterProvider != null;
    this.adapterProvider = adapterProvider;
    assert realmPresenterProvider != null;
    this.realmPresenterProvider = realmPresenterProvider;
  }

  @Override
  public void injectMembers(ArtistListFragment instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    supertypeInjector.injectMembers(instance);
    instance.presenter = presenterProvider.get();
    instance.adapter = adapterProvider.get();
    instance.realmPresenter = realmPresenterProvider.get();
  }

  public static MembersInjector<ArtistListFragment> create(MembersInjector<BaseFragment> supertypeInjector, Provider<ArtistPresenter> presenterProvider, Provider<ArtistAdapter> adapterProvider, Provider<RealmPresenter> realmPresenterProvider) {  
      return new ArtistListFragment_MembersInjector(supertypeInjector, presenterProvider, adapterProvider, realmPresenterProvider);
  }
}

