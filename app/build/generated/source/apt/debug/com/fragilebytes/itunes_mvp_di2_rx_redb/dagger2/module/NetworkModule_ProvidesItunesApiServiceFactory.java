package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.retrofit.ItunesApiService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit.Retrofit;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvidesItunesApiServiceFactory implements Factory<ItunesApiService> {
  private final NetworkModule module;
  private final Provider<Retrofit> retrofitProvider;

  public NetworkModule_ProvidesItunesApiServiceFactory(NetworkModule module, Provider<Retrofit> retrofitProvider) {  
    assert module != null;
    this.module = module;
    assert retrofitProvider != null;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public ItunesApiService get() {  
    ItunesApiService provided = module.providesItunesApiService(retrofitProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ItunesApiService> create(NetworkModule module, Provider<Retrofit> retrofitProvider) {  
    return new NetworkModule_ProvidesItunesApiServiceFactory(module, retrofitProvider);
  }
}

