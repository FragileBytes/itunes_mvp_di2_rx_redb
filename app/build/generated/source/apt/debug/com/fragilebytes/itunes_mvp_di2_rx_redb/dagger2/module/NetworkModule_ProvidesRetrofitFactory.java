package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit.Retrofit;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkModule_ProvidesRetrofitFactory implements Factory<Retrofit> {
  private final NetworkModule module;
  private final Provider<OkHttpClient> clientProvider;
  private final Provider<Gson> gsonProvider;

  public NetworkModule_ProvidesRetrofitFactory(NetworkModule module, Provider<OkHttpClient> clientProvider, Provider<Gson> gsonProvider) {  
    assert module != null;
    this.module = module;
    assert clientProvider != null;
    this.clientProvider = clientProvider;
    assert gsonProvider != null;
    this.gsonProvider = gsonProvider;
  }

  @Override
  public Retrofit get() {  
    Retrofit provided = module.providesRetrofit(clientProvider.get(), gsonProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<Retrofit> create(NetworkModule module, Provider<OkHttpClient> clientProvider, Provider<Gson> gsonProvider) {  
    return new NetworkModule_ProvidesRetrofitFactory(module, clientProvider, gsonProvider);
  }
}

