package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.content.Context;
import dagger.internal.Factory;
import io.realm.Realm;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class RealmModule_ProvideRealmFactory implements Factory<Realm> {
  private final RealmModule module;
  private final Provider<Context> contextProvider;

  public RealmModule_ProvideRealmFactory(RealmModule module, Provider<Context> contextProvider) {  
    assert module != null;
    this.module = module;
    assert contextProvider != null;
    this.contextProvider = contextProvider;
  }

  @Override
  public Realm get() {  
    Realm provided = module.provideRealm(contextProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<Realm> create(RealmModule module, Provider<Context> contextProvider) {  
    return new RealmModule_ProvideRealmFactory(module, contextProvider);
  }
}

