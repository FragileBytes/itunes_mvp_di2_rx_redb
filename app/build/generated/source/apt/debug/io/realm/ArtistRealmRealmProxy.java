package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealm;
import io.realm.RealmFieldType;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ArtistRealmRealmProxy extends ArtistRealm
    implements RealmObjectProxy {

    static final class ArtistRealmColumnInfo extends ColumnInfo {

        public final long artistIndex;
        public final long AlbumIndex;
        public final long songIndex;
        public final long artworkUrl100Index;
        public final long trackIdIndex;

        ArtistRealmColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(5);
            this.artistIndex = getValidColumnIndex(path, table, "ArtistRealm", "artist");
            indicesMap.put("artist", this.artistIndex);

            this.AlbumIndex = getValidColumnIndex(path, table, "ArtistRealm", "Album");
            indicesMap.put("Album", this.AlbumIndex);

            this.songIndex = getValidColumnIndex(path, table, "ArtistRealm", "song");
            indicesMap.put("song", this.songIndex);

            this.artworkUrl100Index = getValidColumnIndex(path, table, "ArtistRealm", "artworkUrl100");
            indicesMap.put("artworkUrl100", this.artworkUrl100Index);

            this.trackIdIndex = getValidColumnIndex(path, table, "ArtistRealm", "trackId");
            indicesMap.put("trackId", this.trackIdIndex);

            setIndicesMap(indicesMap);
        }
    }

    private final ArtistRealmColumnInfo columnInfo;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("artist");
        fieldNames.add("Album");
        fieldNames.add("song");
        fieldNames.add("artworkUrl100");
        fieldNames.add("trackId");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    ArtistRealmRealmProxy(ColumnInfo columnInfo) {
        this.columnInfo = (ArtistRealmColumnInfo) columnInfo;
    }

    @Override
    @SuppressWarnings("cast")
    public String getArtist() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(columnInfo.artistIndex);
    }

    @Override
    public void setArtist(String value) {
        realm.checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field artist to null.");
        }
        row.setString(columnInfo.artistIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String getAlbum() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(columnInfo.AlbumIndex);
    }

    @Override
    public void setAlbum(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(columnInfo.AlbumIndex);
            return;
        }
        row.setString(columnInfo.AlbumIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String getSong() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(columnInfo.songIndex);
    }

    @Override
    public void setSong(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(columnInfo.songIndex);
            return;
        }
        row.setString(columnInfo.songIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String getArtworkUrl100() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(columnInfo.artworkUrl100Index);
    }

    @Override
    public void setArtworkUrl100(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(columnInfo.artworkUrl100Index);
            return;
        }
        row.setString(columnInfo.artworkUrl100Index, value);
    }

    @Override
    @SuppressWarnings("cast")
    public long getTrackId() {
        realm.checkIfValid();
        return (long) row.getLong(columnInfo.trackIdIndex);
    }

    @Override
    public void setTrackId(long value) {
        realm.checkIfValid();
        row.setLong(columnInfo.trackIdIndex, value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_ArtistRealm")) {
            Table table = transaction.getTable("class_ArtistRealm");
            table.addColumn(RealmFieldType.STRING, "artist", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Album", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "song", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "artworkUrl100", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "trackId", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("artist"));
            table.setPrimaryKey("artist");
            return table;
        }
        return transaction.getTable("class_ArtistRealm");
    }

    public static ArtistRealmColumnInfo validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_ArtistRealm")) {
            Table table = transaction.getTable("class_ArtistRealm");
            if (table.getColumnCount() != 5) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 5 but was " + table.getColumnCount());
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < 5; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final ArtistRealmColumnInfo columnInfo = new ArtistRealmColumnInfo(transaction.getPath(), table);

            if (!columnTypes.containsKey("artist")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'artist' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("artist") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'artist' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.artistIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'artist' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'artist' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("artist")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'artist' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("artist"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'artist' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("Album")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'Album' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Album") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'Album' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.AlbumIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'Album' is required. Either set @Required to field 'Album' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("song")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'song' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("song") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'song' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.songIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'song' is required. Either set @Required to field 'song' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("artworkUrl100")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'artworkUrl100' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("artworkUrl100") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'artworkUrl100' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.artworkUrl100Index)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'artworkUrl100' is required. Either set @Required to field 'artworkUrl100' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("trackId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'trackId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("trackId") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'long' for field 'trackId' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.trackIdIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'trackId' does support null values in the existing Realm file. Use corresponding boxed type for field 'trackId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The ArtistRealm class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_ArtistRealm";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static ArtistRealm createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        ArtistRealm obj = null;
        if (update) {
            Table table = realm.getTable(ArtistRealm.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("artist")) {
                long rowIndex = table.findFirstString(pkColumnIndex, json.getString("artist"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new ArtistRealmRealmProxy(realm.schema.getColumnInfo(ArtistRealm.class));
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            if (json.has("artist")) {
                if (json.isNull("artist")) {
                    obj = realm.createObject(ArtistRealm.class, null);
                } else {
                    obj = realm.createObject(ArtistRealm.class, json.getString("artist"));
                }
            } else {
                obj = realm.createObject(ArtistRealm.class);
            }
        }
        if (json.has("artist")) {
            if (json.isNull("artist")) {
                obj.setArtist(null);
            } else {
                obj.setArtist((String) json.getString("artist"));
            }
        }
        if (json.has("Album")) {
            if (json.isNull("Album")) {
                obj.setAlbum(null);
            } else {
                obj.setAlbum((String) json.getString("Album"));
            }
        }
        if (json.has("song")) {
            if (json.isNull("song")) {
                obj.setSong(null);
            } else {
                obj.setSong((String) json.getString("song"));
            }
        }
        if (json.has("artworkUrl100")) {
            if (json.isNull("artworkUrl100")) {
                obj.setArtworkUrl100(null);
            } else {
                obj.setArtworkUrl100((String) json.getString("artworkUrl100"));
            }
        }
        if (json.has("trackId")) {
            if (json.isNull("trackId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field trackId to null.");
            } else {
                obj.setTrackId((long) json.getLong("trackId"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    public static ArtistRealm createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        ArtistRealm obj = realm.createObject(ArtistRealm.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("artist")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setArtist(null);
                } else {
                    obj.setArtist((String) reader.nextString());
                }
            } else if (name.equals("Album")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setAlbum(null);
                } else {
                    obj.setAlbum((String) reader.nextString());
                }
            } else if (name.equals("song")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setSong(null);
                } else {
                    obj.setSong((String) reader.nextString());
                }
            } else if (name.equals("artworkUrl100")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setArtworkUrl100(null);
                } else {
                    obj.setArtworkUrl100((String) reader.nextString());
                }
            } else if (name.equals("trackId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field trackId to null.");
                } else {
                    obj.setTrackId((long) reader.nextLong());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static ArtistRealm copyOrUpdate(Realm realm, ArtistRealm object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        ArtistRealm realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(ArtistRealm.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (object.getArtist() == null) {
                throw new IllegalArgumentException("Primary key value must not be null.");
            }
            long rowIndex = table.findFirstString(pkColumnIndex, object.getArtist());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new ArtistRealmRealmProxy(realm.schema.getColumnInfo(ArtistRealm.class));
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static ArtistRealm copy(Realm realm, ArtistRealm newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        ArtistRealm realmObject = realm.createObject(ArtistRealm.class, newObject.getArtist());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setArtist(newObject.getArtist());
        realmObject.setAlbum(newObject.getAlbum());
        realmObject.setSong(newObject.getSong());
        realmObject.setArtworkUrl100(newObject.getArtworkUrl100());
        realmObject.setTrackId(newObject.getTrackId());
        return realmObject;
    }

    public static ArtistRealm createDetachedCopy(ArtistRealm realmObject, int currentDepth, int maxDepth, Map<RealmObject, CacheData<RealmObject>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<ArtistRealm> cachedObject = (CacheData) cache.get(realmObject);
        ArtistRealm standaloneObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return cachedObject.object;
            } else {
                standaloneObject = cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            standaloneObject = new ArtistRealm();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmObject>(currentDepth, standaloneObject));
        }
        standaloneObject.setArtist(realmObject.getArtist());
        standaloneObject.setAlbum(realmObject.getAlbum());
        standaloneObject.setSong(realmObject.getSong());
        standaloneObject.setArtworkUrl100(realmObject.getArtworkUrl100());
        standaloneObject.setTrackId(realmObject.getTrackId());
        return standaloneObject;
    }

    static ArtistRealm update(Realm realm, ArtistRealm realmObject, ArtistRealm newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setAlbum(newObject.getAlbum());
        realmObject.setSong(newObject.getSong());
        realmObject.setArtworkUrl100(newObject.getArtworkUrl100());
        realmObject.setTrackId(newObject.getTrackId());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ArtistRealm = [");
        stringBuilder.append("{artist:");
        stringBuilder.append(getArtist());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Album:");
        stringBuilder.append(getAlbum() != null ? getAlbum() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{song:");
        stringBuilder.append(getSong() != null ? getSong() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{artworkUrl100:");
        stringBuilder.append(getArtworkUrl100() != null ? getArtworkUrl100() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{trackId:");
        stringBuilder.append(getTrackId());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArtistRealmRealmProxy aArtistRealm = (ArtistRealmRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aArtistRealm.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aArtistRealm.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aArtistRealm.row.getIndex()) return false;

        return true;
    }

}
