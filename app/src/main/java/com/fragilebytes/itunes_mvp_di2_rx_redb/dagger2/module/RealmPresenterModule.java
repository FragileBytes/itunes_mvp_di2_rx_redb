package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.RealmPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 */
@Singleton
@Module
public class RealmPresenterModule {

    public static final String TAG = RealmPresenterModule.class.getSimpleName();

    public RealmPresenterModule() {
    }

    @Provides
    public RealmPresenter providePresenter(RealmInteractor interactor) {
        Log.i(TAG, "providePresenter: ");
        return new RealmPresenter(interactor);
    }
}
