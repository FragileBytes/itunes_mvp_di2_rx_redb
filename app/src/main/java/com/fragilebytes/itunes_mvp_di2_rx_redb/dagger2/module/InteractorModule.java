package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.retrofit.ItunesApiService;

import dagger.Module;
import dagger.Provides;

/**
 * This module returns the interactor to do the job for the Presenter
 *
 * At the moment no constructor with parameters needed
 */
@Module
public class InteractorModule {

    private static final String TAG = InteractorModule.class.getSimpleName();

    /**
     * Provides the Interactor
     * @param apiService Retrofit interface to connect with Api
     * @return
     */
    @Provides
    public ArtistInteractor provideInteractor(ItunesApiService apiService) {
        Log.i(TAG, "provideInteractor: ");
        return new ArtistInteractor(apiService);
    }

}
