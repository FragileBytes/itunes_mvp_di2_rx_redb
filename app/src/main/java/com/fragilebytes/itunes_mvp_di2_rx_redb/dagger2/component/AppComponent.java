package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.scope.AppScope;

import dagger.Component;

/**
 * As Artist SearchPresenter has 2 dependencies we need to make them extend this class
 * in order to avoid an error (1 component cannot have 2 different dependencies)
 */
@AppScope
@Component
public interface AppComponent {
}
