package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.InteractorModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;

import dagger.Component;

/**
 * NetworkComponent requires:
 * -NetworkModule
 * -InteractorModule
 */

@Component (
        modules = {
                NetworkModule.class,
                InteractorModule.class
        }
)
public interface NetworkComponent extends AppComponent{


        ArtistInteractor getArtistInteractor();

}
