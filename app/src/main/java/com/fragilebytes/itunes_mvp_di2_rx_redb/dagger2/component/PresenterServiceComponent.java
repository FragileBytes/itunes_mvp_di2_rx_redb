package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.PresenterServiceModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.scope.ServiceScope;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.service.ArtistService;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;

import dagger.Component;

/**
 */
@ServiceScope
@Component(
        dependencies = NetworkComponent.class,
        modules = PresenterServiceModule.class

)
public interface PresenterServiceComponent {

    void inject(ArtistService service);
    ArtistPresenter getPresenter();
}
