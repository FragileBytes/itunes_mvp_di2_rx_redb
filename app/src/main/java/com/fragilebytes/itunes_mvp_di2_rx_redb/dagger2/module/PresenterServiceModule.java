package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 */
@Singleton
@Module
public class PresenterServiceModule {

    private static final String TAG = PresenterServiceModule.class.getSimpleName();


    @Provides
    ArtistPresenter providePresenter(ArtistInteractor interactor) {
        Log.i(TAG, "providePresenter: with Interactor");
        return new ArtistPresenter(interactor);
    }
}
