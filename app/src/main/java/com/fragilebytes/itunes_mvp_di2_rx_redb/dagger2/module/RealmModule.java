package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.content.Context;
import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealmDAO;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 */

@Module
public class RealmModule {

    private static final String TAG = RealmModule.class.getSimpleName();

    @Provides
    public Realm provideRealm(Context context) {
        Log.i(TAG, "provideRealm: ");
        RealmConfiguration config = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(config);
        return Realm.getDefaultInstance();
    }

    @Provides
    public ArtistRealmDAO provideArtistRealmDAO(Realm realm) {
        Log.i(TAG, "provideArtistRealmDAO: ");
        return new ArtistRealmDAO(realm);
    }
}
