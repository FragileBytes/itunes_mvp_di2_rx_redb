package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}
