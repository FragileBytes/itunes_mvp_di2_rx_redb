package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import dagger.Module;
import dagger.Provides;

/**
 */
@Module
public class ApplicationModule {

    private static final String TAG = ApplicationModule.class.getSimpleName();
    Application app;

    public ApplicationModule(Application app) {
        Log.i(TAG, "ApplicationModule: Constructor");
        this.app = app;
    }

    @Provides
    public Application provideApplication() {
        Log.i(TAG, "provideApplication: ");
        return app;
    }

    @Provides
    public Context provideContext() {
        Log.i(TAG, "provideContext: ");
        return app.getApplicationContext();
    }
}
