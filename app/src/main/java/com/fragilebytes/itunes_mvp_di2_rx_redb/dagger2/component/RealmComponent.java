package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmInteractorModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;

import dagger.Component;

/**
 */

@Component(
        modules = {
                RealmModule.class,
                RealmInteractorModule.class,
                ApplicationModule.class
        }
)
public interface RealmComponent extends AppComponent{

    RealmInteractor getRealmInteractor();

}
