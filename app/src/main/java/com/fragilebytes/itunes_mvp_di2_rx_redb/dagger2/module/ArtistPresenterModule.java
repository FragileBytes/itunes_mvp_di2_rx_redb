package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.content.Context;
import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.ArtistInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter.ArtistAdapter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.viewmodel.ArtistViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 */
@Singleton
@Module
public class ArtistPresenterModule {

    private static final String TAG = ArtistPresenterModule.class.getSimpleName();
    private ArtistViewModel viewModel;


    public ArtistPresenterModule(ArtistViewModel viewModel) {
        Log.i(TAG, "ArtistPresenterModule: Constructor");
        this.viewModel = viewModel;
    }

    @Provides ArtistViewModel provideViewModel() {
        return viewModel;
    }

    @Provides
    public ArtistPresenter providePresenter(ArtistInteractor interactor, ArtistViewModel viewModel) {
        Log.i(TAG, "providePresenter: with Interactor and ViewModel");
        return new ArtistPresenter(interactor, viewModel);
    }


    @Provides
    public ArtistAdapter provideAdapter(Context context) {
        Log.i(TAG, "provideAdapter: ");
        return new ArtistAdapter(context);
    }
}
