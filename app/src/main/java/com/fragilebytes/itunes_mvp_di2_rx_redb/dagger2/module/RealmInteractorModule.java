package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module;

import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealmDAO;

import dagger.Module;
import dagger.Provides;

/**
 */
@Module
public class RealmInteractorModule {

    private static final String TAG = RealmInteractorModule.class.getSimpleName();

    @Provides
    public RealmInteractor provideRealmInteractor(ArtistRealmDAO realmDAO) {
        Log.i(TAG, "provideRealmInteractor: ");
        return new RealmInteractor(realmDAO);
    }
}
