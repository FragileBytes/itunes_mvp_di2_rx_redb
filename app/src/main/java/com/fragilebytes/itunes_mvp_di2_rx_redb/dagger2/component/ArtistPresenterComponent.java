package com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ArtistPresenterModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmPresenterModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.scope.ActivityScope;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.RealmPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.activity.MainActivity;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.adapter.ArtistAdapter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment.ArtistListFragment;

import dagger.Component;

/**
 * Component that will give access to the Presenter
 * We define the scope of this component for the Activity
 * Here we define where are we going to inject the Presenter
 */

@ActivityScope
@Component(
        dependencies = {
                NetworkComponent.class,
                RealmComponent.class
        },
        modules = {
                ArtistPresenterModule.class,
                ApplicationModule.class,
                RealmPresenterModule.class //
        }
)
public interface ArtistPresenterComponent extends NetworkComponent, RealmComponent  {

        void inject(MainActivity activity);
        void inject(ArtistListFragment fragment);

        ArtistPresenter getPresenter();
        ArtistAdapter getAdapter();
        RealmPresenter getRealmPresenter(); //


}
