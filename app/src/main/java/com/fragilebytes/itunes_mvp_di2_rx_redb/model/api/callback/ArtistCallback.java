package com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.callback;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.itunesmodel.Result;

import java.util.List;

/**
 *
 * Callback to communicate the results to ArtistSearchPresenter
 */
public interface ArtistCallback extends ApiCallback{

    void onArtistsFound(List<Result> artists); // TODO maybe we dont need all the atributes of the object, in that case --> customize loca object

    void onFailedSearch();

}
