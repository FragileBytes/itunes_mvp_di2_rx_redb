package com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.callback;

/**
 * Callback used to communicate Presenter and Interactor with the data needed in Presenter
 *
 */
public interface ApiCallback {

    void onNetworkError();
    void onApiError();
}
