package com.fragilebytes.itunes_mvp_di2_rx_redb.model.api.retrofit;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.itunesmodel.Itunes;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Connect to the RestApi
 */
public interface ItunesApiService {

    @GET(NetworkConstants.GENDER_ENDPOINT)
    Observable<Itunes> search(@Query(NetworkConstants.QUERY_TERM) String query);
//    Call<Itunes> search(@Query(NetworkConstants.QUERY_TERM) String query);
}
