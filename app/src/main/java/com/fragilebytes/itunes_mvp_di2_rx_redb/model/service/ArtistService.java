package com.fragilebytes.itunes_mvp_di2_rx_redb.model.service;

import android.content.Context;
import android.content.Intent;

import com.fragilebytes.itunes_mvp_di2_rx_redb.constants.Constants;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.DaggerPresenterServiceComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.NetworkComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.PresenterServiceModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.presenter.ArtistPresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BasePresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BaseService;

import javax.inject.Inject;

/**
 */
public class ArtistService extends BaseService {

    @Inject
    ArtistPresenter presenter;

    public static Intent makeIntent(Context context, String query) {
        return new Intent(context, ArtistService.class).putExtra(Constants.EXTRA_QUERY_GENDER, query);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void setUpComponent(NetworkComponent component) {
         DaggerPresenterServiceComponent.builder()
                .networkComponent(component)
                .presenterServiceModule(new PresenterServiceModule())
                .build().inject(this);

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        presenter.search(intent.getStringExtra(Constants.EXTRA_QUERY_GENDER));
    }
}
