package com.fragilebytes.itunes_mvp_di2_rx_redb.interactor;

import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.itunesmodel.Result;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealm;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.realm.ArtistRealmDAO;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class RealmInteractor {

    private static final String TAG = RealmInteractor.class.getSimpleName();
    private ArtistRealmDAO realmDAO;

    public RealmInteractor(ArtistRealmDAO realmDAO) {
        Log.i(TAG, "RealmInteractor: ");
        this.realmDAO = realmDAO;
    }

    private List<ArtistRealm> convertResultToArtistRealm(List<Result> results) {
        List<ArtistRealm> artistRealms = new ArrayList<>(results.size());
        for (Result result : results) {
            ArtistRealm realm = new ArtistRealm();
            realm.setArtist(result.getArtistName());
            realm.setAlbum(result.getCollectionName());
            realm.setSong(result.getTrackName());
            realm.setArtworkUrl100(result.getArtworkUrl100());
            realm.setTrackId(result.getTrackId());
            artistRealms.add(realm);
        }
        return artistRealms;
    }

    public void saveAllArtist(List<Result> artists) {
        Log.i(TAG, "saveAllArtist: ");
        realmDAO.saveAllArtists(convertResultToArtistRealm(artists));

    }

    public void queryAllArtist() { // TODO REFACTOR
        Log.i(TAG, "queryAllArtist: ");
         realmDAO.queryAll();
    }

}
