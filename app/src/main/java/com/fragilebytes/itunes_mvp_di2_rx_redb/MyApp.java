package com.fragilebytes.itunes_mvp_di2_rx_redb;

import android.app.Application;
import android.content.Context;

import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.DaggerNetworkComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.DaggerRealmComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.NetworkComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.RealmComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.ApplicationModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.NetworkModule;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.module.RealmModule;

/**
 */
public class MyApp extends Application {

    private NetworkComponent networkComponent;
    private RealmComponent realmComponent;

    public static MyApp getApp(Context context) {
        return ((MyApp) context.getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setUpNetworkComponent();
        setUpRealmComponent();

    }

    private void setUpRealmComponent() {
        realmComponent = DaggerRealmComponent.builder()
                .applicationModule(new ApplicationModule(this))
//                .realmInteractorModule(new RealmInteractorModule())
                .realmModule(new RealmModule())
                .build();
    }

    private void setUpNetworkComponent() {
        networkComponent = DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule())
//                .interactorModule(new InteractorModule())
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }

    public RealmComponent getRealmComponent() {
        return realmComponent;
    }
}