package com.fragilebytes.itunes_mvp_di2_rx_redb.view.base;

import android.app.IntentService;

import com.fragilebytes.itunes_mvp_di2_rx_redb.MyApp;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.NetworkComponent;

/**
 */
public abstract class BaseService extends IntentService {

    private static final String TAG = BaseService.class.getSimpleName();

    public BaseService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        injectDependencies();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    protected abstract BasePresenter getPresenter();

    protected abstract void setUpComponent(NetworkComponent component);

    private void injectDependencies() {
        setUpComponent(MyApp.getApp(getApplicationContext()).getNetworkComponent());
    }

}
