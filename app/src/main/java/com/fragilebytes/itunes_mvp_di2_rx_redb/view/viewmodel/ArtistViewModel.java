package com.fragilebytes.itunes_mvp_di2_rx_redb.view.viewmodel;

import com.fragilebytes.itunes_mvp_di2_rx_redb.model.itunesmodel.Result;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.activity.MainActivity;

import java.util.List;

/**
 * This is the view model, it defines the behaview of the View in the MVP
 * This interface operations must be used to update the UI in any activity
 * like {@link MainActivity}
 */
public interface ArtistViewModel {

    void setupList();

    void setupAdapter();

    void displayFoundArtists(List<Result> artists); // TODO create a model for the bands to display in the RV adapter

    void displayFailedSearch();

    void displayNetworkError();

    void displayApiError();

}
