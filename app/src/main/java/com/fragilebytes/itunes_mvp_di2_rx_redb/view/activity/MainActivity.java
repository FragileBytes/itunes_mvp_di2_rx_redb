package com.fragilebytes.itunes_mvp_di2_rx_redb.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.fragilebytes.itunes_mvp_di2_rx_redb.R;
import com.fragilebytes.itunes_mvp_di2_rx_redb.dagger2.component.NetworkComponent;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BaseActivity;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.base.BasePresenter;
import com.fragilebytes.itunes_mvp_di2_rx_redb.view.fragment.ArtistListFragment;

import butterknife.BindString;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindString(R.string.fragment_artist_list) String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        displayFragment(ArtistListFragment.newInstance(), tag);
    }

    private void displayFragment(Fragment fragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, fragment, tag).commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Nullable
    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void setUpComponent(NetworkComponent component) {
    }

}
