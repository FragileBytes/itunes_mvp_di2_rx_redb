package com.fragilebytes.itunes_mvp_di2_rx_redb.presenter;

import android.util.Log;

import com.fragilebytes.itunes_mvp_di2_rx_redb.interactor.RealmInteractor;
import com.fragilebytes.itunes_mvp_di2_rx_redb.model.itunesmodel.Result;

import java.util.List;


public class RealmPresenter {

    private static final String TAG = RealmPresenter.class.getSimpleName();
    private RealmInteractor interactor;

    public RealmPresenter(RealmInteractor interactor) {
        Log.i(TAG, "RealmPresenter: COnstructor with realm interactor");
        this.interactor = interactor;
    }

    public void saveAllResults(List<Result> results) {
        Log.i(TAG, "saveAllResults: ");
        interactor.saveAllArtist(results);
    }

    public void queryAll() {
        interactor.queryAllArtist();
    }
}
